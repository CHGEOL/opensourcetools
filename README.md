![](Logo.png)

# Geologie Open Source Tools
### Eine Linksammlung des Schweizer Geologinnen und Geologenverbandes CHGEOL

</br>

**ENTWURF**, diese Liste befindet sich noch in Bearbeitung. Kommentare, Ideen oder Anregungen sind herzlich willkommen und können per E-Mail an michael.koebberich@chgeol.ch gesendet werden.

---



## **3D Modellierung und Visualisierung**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> Blender
C und C++ basierte Anwendung zur 3D-Modellierung.

[Webseite](https://www.blender.org/), [Download](https://www.blender.org/download/), [Quellcode](https://github.com/blender/blender), [Dokumentation](https://docs.blender.org/manual/en/latest/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> GemPy 
Python und VTK basierte Programmbibliothek zur 3D-Modellierung des geologischen Untergrundes, einschliesslich Faltungen und Störungszonen.

[Webseite](https://www.gempy.org/), [Quellcode](https://github.com/cgre-aachen/gempy)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> HyVR
Python basierte Programmbibliothek zur Erzeugung anisotroper sedimentärer geologischer Untergrundmodelle.

[Quellcode](https://github.com/driftingtides/hyvr), [Dokumentation](https://github.com/driftingtides/hyvr), [Artikel](https://ngwa.onlinelibrary.wiley.com/doi/abs/10.1111/gwat.12803)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> OpenGeode 
C++ basierte Programmbibliothek zur Darstellung und Manipulation geometrischer Modelle von einfachen Meshes hin zu komplexeren Boundary Representations.

[Webseite](https://geode-solutions.com/opengeode/), [Download](https://www.orfeo-toolbox.org/download/), [Quellcode](https://github.com/Geode-solutions/OpenGeode), [Dokumentation](https://docs.geode-solutions.com/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Paraview
C++, Python und VTK basierte betriebssystem-unabhängige Anwendung zur Analyse und Visualisierung räumlicher Daten. 

[Webseite](https://www.paraview.org/), [Quellcode](https://github.com/Kitware/ParaView), [Dokumentation](https://docs.paraview.org/en/latest/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> PyVista 
Python und VTK basierte Programmbibliothek zur Berechnung von Vermaschungen für die 3D-Visualisierung und deren Darstellung.

[Webseite](https://www.pyvista.org/), [Quellcode](https://github.com/pyvista)





## **Bohrungen und Sondierungen**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> lasio 
Python und VTK basierte Programmbibliothek zum Lesen und Schreiben von Bohrdaten im Log ASCII Standard (LAS) Dateiformat.

[Webseite](https://pypi.org/project/lasio/), [Quellcode](https://github.com/kinverarity1/lasio/), [Dokumentation](https://lasio.readthedocs.io/en/latest/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Striplog 
Python basierte Programmbibliothek zur Darstellung lithologischer und stratigraphischer Bohr- und Aufschlussprofile. 

[Webseite](https://agilescientific.com/blog/2015/4/15/striplog), [Quellcode](https://github.com/agile-geoscience/striplog), [Dokumentation](https://striplog.readthedocs.io/en/latest/)


## **Chemische Reaktionsmodellierung**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> PhreeQC 
C++ basierte Programmbibliothek zur Berechnung von thermodynamischen Lösungsgleichgewichten in wässrigen Lösungen.

[Webseite](https://pubs.er.usgs.gov/publication/tm6A43), [Quellcode](https://www.usgs.gov/software/phreeqc-version-3), [Documentation](https://pubs.usgs.gov/tm/06/a43/pdf/tm6-A43.pdf)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Reaktoro 
C++ und Python basiertes Umgebung zur themodynamischen und kinetischen Modellierung chemisch reaktive Systeme. 

[Webseite](https://reaktoro.org/), [Quellcode](https://github.com/reaktoro/reaktoro), [Dokumentation](https://reaktoro.org/cpp/index.html)



## **Datenanalyse**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Pandas 
Python basierte Programmbibliothek zur effizienten Datenanalyse und zur Manipulation von Daten, einschliesslich deren Visualisierung.

[Webseite](https://pandas.pydata.org/), [Quellcode](https://github.com/pandas-dev/pandas)



## **Erdoberflächendynamik**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Landlab 
Python und Jupyter Notebook basierte Programmbibliothek zur numerischen Modellierung dynamischer Prozesse an der Erdoberfläche.

[Quellcode](https://github.com/landlab/landlab), [Dokumentation](https://landlab.readthedocs.io/en/master/), [Artikel](https://esurf.copernicus.org/articles/5/21/2017/)



## **Geophysik**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Devido
Python basierte Programmbibliothek zur symbolischen Berechnung finiter Differenzen zur seismischen Inversion.

[Webseite](https://www.devitoproject.org/index.html), [Quellcode](https://github.com/devitocodes/devito), [Dokumentation](https://www.devitoproject.org/devito/index.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> gprMax
Python basierte Programmbibliothek zur Simulation der Ausbreitung elektromagnetischer Wellen von Bodenradarmessungen (Ground Penetrating Radar).

[Webseite](https://simpeg.xyz/), [Quellcode](https://github.com/gprmax/gprMax), [Dokumentation](http://docs.gprmax.com/en/latest/), [Artikel](http://dx.doi.org/10.1016/j.cpc.2016.08.020)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> pyGIMLi 
Python basierte Programmbibliothek zur Modellierung, Inversion und Visualisierung geophysikalischer Daten.

[Webseite](https://www.pygimli.org/), [Quellcode](https://github.com/gimli-org/gimli)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> SimPEG 
Python basierte Programmbibliothek zur Simulation und Signifikanzanalyse geophysikalischer Parameter. 

[Webseite](https://simpeg.xyz/), [Quellcode](https://github.com/simpeg/simpeg), [Dokumentation](http://docs.simpeg.xyz)



## **Geostatistik**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> gstat
C basierte Programmbibliothek zur geostatistischen Modellierung, Vorhersage und Simulation räumlich und zeitlich aufgelöster Daten.  

[Webseite](https://r-spatial.github.io/gstat/), [Quellcode](https://github.com/r-spatial/gstat), [Dokumentation](https://r-spatial.github.io/gstat/reference/index.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> pyKriging 
Python basierte Programmbibliothek zur geostatistischen Interpolation zwischen spärlich verteilten Datenpunkten mithilfe der Kriging Methode.

[Webseite](http://www.pykriging.com), [Quellcode](https://github.com/capaulson/pyKriging)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> pysgems 
C++ und Python basierte Programmbibliothek zum Einbinden der Software SGeMS in Python.

[Webseite](http://sgems.sourceforge.net/?q=node/13), [Quellcode](https://github.com/robinthibaut/pysgems)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> rasterstats 
Python basierte Programmbibliothek zum Berechnung statistischer Parameter von vektoriell definierten Ausschnitten in Rasterdatensätzen.

[Quellcode](https://github.com/perrygeo/python-rasterstats/), [Download](https://github.com/mogasw/rasterix/releases/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> SciPy 
Python, Fortran, C und C++ basierte Programmbibliothek für mathematische Anwendungen in Python, inkl. Statistik, Optimierung, ODEs und vielem mehr.

[Webseite](https://scipy.org/scipylib/), [Download](https://scipy.org/scipylib/download.html), [Quellcode](https://github.com/scipy/scipy), [Dokumentation](https://docs.scipy.org/doc/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> SGeMS 
C++ basierte Software zur geostatistischen Modellierung.

[Webseite](http://sgems.sourceforge.net/), [Quellcode](https://sourceforge.net/projects/sgems/), [Download](http://sgems.sourceforge.net/?q=node/77)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> SimPEG 
Python basierte Programmbibliothek zur Simulation und Signifikanzanalyse geophysikalischer Parameter. 

[Webseite](https://simpeg.xyz/), [Quellcode](https://github.com/simpeg/simpeg), [Dokumentation](http://docs.simpeg.xyz)



## **Grundwassermodellierung**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> ModFlow 
C++ basierte Programmbibliothek zur finite-difference Modellierung von Grundwasserströmungen und der Interactionen von Grundwasser mit Oberflächen. 

[Webseite](https://www.usgs.gov/software/modflow-6-usgs-modular-hydrologic-model), [Download/Quellcode](https://water.usgs.gov/water-resources/software/MODFLOW-6/), [Dokumentation](https://water.usgs.gov/water-resources/software/MODFLOW-6/mf6io_6.2.2.pdf)



## **Kartographie**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> GDAL/OGR 
C und C++ basierte Programmbibliothek zur Verarbeitung räumlicher Rasterdaten, einschliesslich deren Georeferenzierung. Das ehemals unabhängige Projekt OGR wird zusammen mit GDAL gepflegt und erweitert dessen Funktionalitäten auf räumliche Vektordaten.

[Webseite](https://gdal.org/), [Quellcode](https://github.com/OSGeo/gdal)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> GRASS GIS 
C und Python basiertes Desktop Geoinformationssystem (GIS), dass Betriebssystem-übergreifend und als 
Docker-Container verfügbar ist.

[Webseite](https://grass.osgeo.org/), [Quellcode](https://github.com/OSGeo/grass), [Download](https://grass.osgeo.org/download/), [Dokumentation](https://grass.osgeo.org/learn/manuals/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> ipyleaflet 
Python basierte Programmbibliothek zur Visualisierung interaktive Karten in Jupyter Notebooks.

[Webseite](https://pypi.org/project/ipyleaflet/), [Quellcode](https://github.com/jupyter-widgets/ipyleaflet), [Dokumentation](https://ipyleaflet.readthedocs.io/en/latest/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> Leaflet 
JavaScript basierte Programmbibliothek zur mobilgeräte-tauglichen Visualisierung von Karten im Browser.

[Webseite](https://leafletjs.com/), [Quellcode](https://github.com/Leaflet/Leaflet), [Dokumentation](https://leafletjs.com/reference-1.7.1.html), [Download](https://leafletjs.com/download.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> QGIS 
C++ basiertes Desktop Geoinformationssystem (GIS) zur Verarbeitung und Visualisierung geographischer Karten und Informationen.

[Webseite](https://qgis.org/en/site/), [Quellcode](https://github.com/qgis/QGIS), [Dokumentation](https://www.qgis.org/en/docs/index.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> rasterix 
C++ basierte Anwendung zur Prozessierung räumlicher Rasterdaten.

[Quellcode](https://github.com/mogasw/rasterix), [Dokumentation](https://pythonhosted.org/rasterstats/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> rasterstats 
Python basierte Programmbibliothek zum Berechnung statistischer Parameter von vektoriell definierten Ausschnitten in Rasterdatensätzen.

[Quellcode](https://github.com/perrygeo/python-rasterstats/), [Download](https://github.com/mogasw/rasterix/releases/)





## **Maschinelles Lernen**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> scikit-learn 
Python und SciPy basierte Programmbibliothek für das maschinelle Lernen in Python.

[Webseite](https://scikit-learn.org/stable/), [Quellcode](https://github.com/scikit-learn/scikit-learn), [Dokumentation](https://scikit-learn.org/stable/user_guide.html), [Tutorial](https://scikit-learn.org/stable/getting_started.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> TensorFlow
C++ und Python basierte Umgebung für das maschinelle Lernen.

[Webseite](tensorflow.org), [Quellcode](https://github.com/tensorflow/tensorflow), [Dokumentation](https://www.tensorflow.org/api_docs)



## **Mathematische Grundlagen**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> NumPy 
C und Python basierte Programmbibliothek zum Umgang mit Vektoren, Matrizen und grossen mehrdimensionalen Arrays.

[Webseite](https://numpy.org/), [Quellcode](https://github.com/numpy/numpy)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> SciPy 
Python, Fortran, C und C++ basierte Programmbibliothek für mathematische Anwendungen in Python, inkl. Statistik, Optimierung, ODEs und vielem mehr.

[Webseite](https://scipy.org/scipylib/), [Download](https://scipy.org/scipylib/download.html), [Quellcode](https://github.com/scipy/scipy), [Dokumentation](https://docs.scipy.org/doc/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> SAGA 
C++ basierte Software zur automatisierten Analyse räumlicher geowissenschaftlicher Daten.

[Webseite](http://www.saga-gis.org/en/index.html), [Quellcode](https://sourceforge.net/projects/saga-gis/files/), [Download](https://sourceforge.net/projects/saga-gis/files/), [Dokumentation](https://sourceforge.net/p/saga-gis/wiki/Home/)




## **Punktwolken**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> CloudCompare 
C++ basierte Programmbibliothek zum Vergleich zweier Punktwolken miteinander für die Visualisierung der Unterschiede. 

[Webseite](https://www.danielgm.net/cc/), [Download](https://www.danielgm.net/cc/release/index.html), [Quellcode](https://github.com/cloudcompare/cloudcompare), [Dokumentation](https://www.danielgm.net/cc/documentation.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> PDAL 
C++ basierte Programmbibliothek zur Verarbeitung und Manipulation von Punktdaten, wie diese zum Beispiel für LiDAR zum Einsatz kommen.

[Webseite](https://pdal.io/), [Quellcode](https://github.com/PDAL/PDAL)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> potree
Javascript basierte Programmbibliothek und Desktop Viewer zur WebGL basierten Darstellung grosser Punktwolken.

[Webseite](potree.org), [Quellcode](https://github.com/potree/potree), [Download](https://github.com/potree/PotreeDesktop/releases)



## **Reaktive Transportmodellierung**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> ModFlow 
C++ basierte Programmbibliothek zur finite-difference Modellierung von Grundwasserströmungen und der Interactionen von Grundwasser mit Oberflächen. 

[Webseite](https://www.usgs.gov/software/modflow-6-usgs-modular-hydrologic-model), [Download/Quellcode](https://water.usgs.gov/water-resources/software/MODFLOW-6/), [Dokumentation](https://water.usgs.gov/water-resources/software/MODFLOW-6/mf6io_6.2.2.pdf)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> PhreeQC 
C++ basierte Programmbibliothek zur Berechnung von thermodynamischen Lösungsgleichgewichten in wässrigen Lösungen.

[Webseite](https://pubs.er.usgs.gov/publication/tm6A43), [Quellcode](https://www.usgs.gov/software/phreeqc-version-3), [Documentation](https://pubs.usgs.gov/tm/06/a43/pdf/tm6-A43.pdf)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Reaktoro 
C++ und Python basiertes Umgebung zur themodynamischen und kinetischen Modellierung chemisch reaktive Systeme. 

[Webseite](https://reaktoro.org/), [Quellcode](https://github.com/reaktoro/reaktoro), [Dokumentation](https://reaktoro.org/cpp/index.html)



## **Strukturgeologie**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> APSG 
Python basierte Programmbibliothek zur Visualisierung räumlich orientierter strukturgeologischer Daten in Stereonetzen und Rosendiagrammen.

[Webseite](https://pypi.org/project/apsg/), [Quellcode](https://github.com/ondrolexa/apsg)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> mplstereonet 
Python basierte Programmbibliothek zur Visualisierung räumlich orientierter strukturgeologischer Daten in Stereonetzen und Rosendiagrammen.

[Webseite](https://pypi.org/project/mplstereonet/), [Quellcode](https://github.com/joferkington/mplstereonet)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Stereonets and Rose Diagrams 
Python basiertes Skript zur Visualisierung räumlich orientierter strukturgeologischer Daten in Stereonetzen und Rosendiagrammen.

[Blog](http://geologyandpython.com/structural_geology.html), [Quellcode](https://zenodo.org/record/888281)



## **Visualisierung**



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> D3.js 
JavaScript basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von Daten im Webbrowser.

[Webseite](https://d3js.org/), [Quellcode](https://github.com/d3/d3), [Dokumentation](https://github.com/d3/d3/wiki)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> GeoJs 
JavaScript basierte Programmbibliothek zur Visualisierung und Interaktion mit georeferenzierte Vektorkarten.

[Webseite](https://opengeoscience.github.io/geojs/), [Quellcode](https://github.com/OpenGeoscience/geojs), [Dokumentation](https://geojs.readthedocs.io/en/latest/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/typescript/typescript.png" height="16"> vis.js (graph2d) 
JavaScript und TypeScript basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von als 2D-Diagramm dargestellten Daten im Webbrowser.

[Webseite](https://visjs.org), [Quellcode](https://github.com/visjs/vis-timeline), [Dokumentation](https://visjs.github.io/vis-timeline/docs/graph2d/), [Beispiele](https://visjs.github.io/vis-timeline/examples/graph2d/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/typescript/typescript.png" height="16"> vis.js (graph3d) 
JavaScript und TypeScript basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von als 3D-Diagramm dargestellten Daten im Webbrowser.

[Webseite](https://visjs.org), [Quellcode](https://github.com/visjs/vis-graph3d), [Dokumentation](https://visjs.github.io/vis-graph3d/docs/graph3d/index.html), [Beispiele](https://visjs.github.io/vis-graph3d/examples/index.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/typescript/typescript.png" height="16"> vis.js (network) 
JavaScript und TypeScript basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von als Netzwerk dargestellten Daten im Webbrowser.

[Webseite](https://visjs.org), [Quellcode](https://github.com/visjs/vis-network), [Dokumentation](https://visjs.github.io/vis-network/docs/network/), [Beispiele](https://visjs.github.io/vis-graph3d/examples/index.html)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/javascript/javascript.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/typescript/typescript.png" height="16"> vis.js (timeline) 
JavaScript und TypeScript basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von als Zeitstrahl dargestellten Daten im Webbrowser.

[Webseite](https://visjs.org), [Quellcode](https://github.com/visjs/vis-timeline), [Dokumentation](https://visjs.github.io/vis-timeline/docs/timeline/), [Beispiele](https://visjs.github.io/vis-timeline/examples/timeline/)



### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/c/c.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> VTK 
C, C++ und Python basierte Programmbibliothek zum rendern von 3D-Grafiken und zur Bildverarbeitung für wissenschaftliche Zwecke.

[Webseite](https://vtk.org/), [Quellcode](https://github.com/Kitware/VTK)



## **Noch unstrukturierte Links**

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> CGAL 
C++ basierte Programmbibliothek, die eine Vielzahl geometrischer Algorithmen beinhaltet (Triangulationen, Voronoi Diagramme, etc.), welche heute die Grundlage für eine Vielzahl der Funktionen in GIS und CAD Systemen darstellen.

[Webseite](https://www.cgal.org/), [Quellcode](https://github.com/CGAL/cgal)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> cmocean 
Python basierte Programmbibliothek zur Visualisierung von Farbschemata zur Erzeugung von Farbverläufen für die Ozeanographie.

[Webseite/Documentation](https://matplotlib.org/cmocean/), [Quellcode](https://github.com/matplotlib/cmocean)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Colorcet 
Python basierte Programmbibliothek zur Visualisierung von Farbschemata zur Erzeugung von Gradienten.

[Webseite](https://colorcet.holoviz.org/#), [Quellcode](https://github.com/holoviz/colorcet), [Dokumentation](https://colorcet.holoviz.org/user_guide/index.html)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> ERT 
Python basierte Programmbibliothek zur Enseamble basierten Analyse von Reservoir Modellen.

[Quellcode](https://github.com/equinor/ert), [Dokumentation](https://ert.readthedocs.io/en/latest/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> GeologicPatterns 
Python und Shell basierte Programmbibliothek zur Visualisierung von Signaturen auf geologischen Karten und Profilen.

[Webseite](https://ngmdb.usgs.gov/fgdc_gds/geolsymstd/download.php), [Quellcode](https://github.com/davenquinn/geologic-patterns), [Dokumentation](https://davenquinn.com/projects/geologic-patterns/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> GeoNotebook 
Python und Jupyter Notebook basierte Programmbibliothek zur Visualisierung und Analyse georeferenzierter in Jupyter Notebooks.

[Quellcode](https://github.com/OpenGeoscience/geonotebook), [Dokumentation](https://geonotebook.readthedocs.io/en/latest/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> GeoPyTool 
Python basierte Software zur Visualisierung geochemischen Datensätze in fachspezifischen Diagrammen.

[Webseite](http://geopytool.com), [Quellcode](https://github.com/GeoPyTool/GeoPyTool), [Download](http://geopytool.com/download.html), [Install](http://geopytool.com/installation-expert.html), [Artikel](https://www.sciencedirect.com/science/article/pii/S1674987118301609)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> pyrolite 
Python basierte Programmbibliothek zur Transformation geochemischer Daten für die Visualisierung, z.B. in ternären Diagrammen.

[Webseite](https://pypi.org/project/pyrolite/), [Quellcode](https://github.com/morganjwilliams/pyrolite), [Dokumentation](https://pyrolite.readthedocs.io/en/main/), [Artikel](https://joss.theoj.org/papers/10.21105/joss.02314)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> mapnik 
C++ basierte Programmbibliothek für das pixel-basierte Rendering geographischer Informationen über C++, Python oder Node Applikationen.

[Webseite](https://mapnik.org/), [Quellcode](https://github.com/mapnik), [Dokumentation](https://github.com/mapnik/mapnik/wiki)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Matplotlib 
Python basierte Programmbibliothek zur Visualisierung von Messergebnissen und Modellresultaten in Form von 2D- und 3D-Grafiken.

[Webseite](https://matplotlib.org/), [Quellcode](https://github.com/matplotlib/matplotlib)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> omfvista 
Python basierte Programmbibliothek zur Visualisierung von Daten im Open Mining Format (OMF) in PyVista.

[Quellcode](https://github.com/OpenGeoVis/omfvista), [Dokumentation](https://opengeovis.github.io/omfvista/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/cpp/cpp.png" height="16"> Orfeo ToolBox 
OpenGL Shading Language (GLSL) und C++ basierte Programmbibliothek zur Verarbeitung und Visualisierung von Remote Sensing Daten.

[Webseite](https://www.orfeo-toolbox.org/), [Download](https://www.orfeo-toolbox.org/download/), [Quellcode](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb), [Dokumentation](https://www.orfeo-toolbox.org/CookBook/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Pangeo 
Python basierte Sammlung von Programmbibliotheken zum Aufbau einer community platform for big data geoscience.

[Webseite](https://pangeo.io/), [Quellcode](https://github.com/pangeo-data/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> PVGeo 
Python basierte Programmbibliothek zur Visualisierung zeitaufgelöster 2D- und 3D-Daten in Paraview oder mithilfe von PyVista in Python. 

[Quellcode](https://github.com/OpenGeoVis/PVGeo), [Dokumentation](https://pvgeo.org/)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> PyGSLIB
Python und Jupyter Notebook basierte Programmbibliothek zur Visualisierung, Interaktion und Manipulation von als 3D-Diagramm dargestellten Daten im Webbrowser.

[Quellcode](https://github.com/opengeostat/pygslib), [Dokumentation](https://opengeostat.github.io/pygslib/index.html), [Wiki](https://github.com/opengeostat/pygslib/wiki), [Videos](https://www.youtube.com/c/opengeostat)

### <img src="https://cdn.jsdelivr.net/npm/programming-languages-logos/src/python/python.png" height="16"> Verde
Python basierte Programmbibliothek zur Verarbeitung und Interpolation räumlicher Daten auf regulären Netzen (regular grids).

[Webseite/Dokumentation](https://www.fatiando.org/verde/latest/), [Quellcode](https://github.com/fatiando/verde)
